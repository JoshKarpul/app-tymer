import React from 'react';
import { StyleSheet, FlatList, Text, View, ScrollView, Picker } from 'react-native';
import { LinearGradient } from 'expo'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hour: [
        {key: '1'},
        {key: '2'},
        {key: '3'},
        {key: '4'},
        {key: '5'},
        {key: '6'},
        {key: '7'},
        {key: '8'},
        {key: '9'},
      ],
    }
  }

  render() {
    return (
        <View style={styles.container}>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1, height: 150, borderWidth: 2, borderColor: 'black'}}>
              <View style= {styles.center}>
                <Text style={{color:'white', paddingBottom: 10}}>Hours</Text>
              </View>
              <LinearGradient
                colors={['#666666', 'white', 'white', 'white', 'black']}>            
                <FlatList
                  style ={styles.picker}
                  data={this.state.hour}
                  showsVerticalScrollIndicator={false}
                  ItemSeparatorComponent={({highlighted}) => (<View style={[styles.separator, highlighted && {marginLeft: 0}]} />)}
                  renderItem={({item}) => 
                    <View style= {styles.center}>
                      <Text style={styles.text}>{item.key}</Text>
                    </View>}/>
              </LinearGradient>
            </View>
            <View style={{flex: 1, height: 150, borderWidth: 2, borderColor: 'black'}}>
              <View style= {styles.center}>
                <Text style={{color:'white', paddingBottom: 10}}>Minutes</Text>
              </View>
              <LinearGradient
                colors={['#666666', 'white', 'white', 'white', 'black']}>            
                <FlatList
                  style ={styles.picker}
                  data={this.state.hour}
                  showsVerticalScrollIndicator={false}
                  ItemSeparatorComponent={({highlighted}) => (<View style={[styles.separator, highlighted && {marginLeft: 0}]} />)}
                  renderItem={({item}) => 
                    <View style= {styles.center}>
                      <Text style={styles.text}>{item.key}</Text>
                    </View>}/>
              </LinearGradient>
            </View>
            <View style={{flex: 1, height: 150, borderWidth: 2, borderColor: 'black'}}>
              <View style= {styles.center}>
                <Text style={{color:'white', paddingBottom: 10}}>Seconds</Text>
              </View>
              <LinearGradient
                colors={['#666666', 'white', 'white', 'white', 'black']}>            
                <FlatList
                  style ={styles.picker}
                  data={this.state.hour}
                  ItemSeparatorComponent={({highlighted}) => (<View style={[styles.separator, highlighted && {marginLeft: 0}]} />)}
                  showsVerticalScrollIndicator={false}
                  snapToInterval={80} //your element width
                  snapToAlignment={"center"}
                  renderItem={({item}) => 
                    <View style= {styles.center}>
                      <Text style={styles.text}>{item.key}</Text>
                    </View>}/>
              </LinearGradient>
            </View>
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center'
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 20
  },
  picker: {
    backgroundColor: 'rgba(0,0,0,0)'
  },
  separator: {
    height: 1,
    backgroundColor: '#eaeaea'
  }
});
